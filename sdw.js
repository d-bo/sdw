
/**
 * SDW - Simple dialog window
 * javascript standalone (no js frameworks required)
 *
 * support IE6 >
 * event handler module taken from javascript.ru
 
 * TODO: 
 * .prompt(), .alert(), .confirm()
 * onClose, onRender, onConfirm events
 * CSS3 modern transitions effects
 * + bugs
 *
 * @author dmitry.b
 */

var sdw = function(config){
	
	if (typeof config != 'object') config = {}
	
	
	/**
	 * IE indexOf fix
	 */
	
	 
	if (!Array.prototype.indexOf) {
	  Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
		'use strict';
		if (this == null) {
		  throw new TypeError();
		}
		var n, k, t = Object(this),
			len = t.length >>> 0;
	
		if (len === 0) {
		  return -1;
		}
		n = 0;
		if (arguments.length > 1) {
		  n = Number(arguments[1]);
		  if (n != n) { // shortcut for verifying if it's NaN
			n = 0;
		  } else if (n != 0 && n != Infinity && n != -Infinity) {
			n = (n > 0 || -1) * Math.floor(Math.abs(n));
		  }
		}
		if (n >= len) {
		  return -1;
		}
		for (k = n >= 0 ? n : Math.max(len - Math.abs(n), 0); k < len; k++) {
		  if (k in t && t[k] === searchElement) {
			return k;
		  }
		}
		return -1;
	  };
	}
	

	/**
	 * Drag event handler crossbrowser
	 */
	
	 
	function DragObject(element, box) {
		element.dragObject = this
		
		if (typeof box == 'object')
		{
			var _box = box
		}
		
		dragMaster.makeDraggable(element)
		
		var rememberPosition
		var mouseOffset
		
		this.onDragStart = function(offset) {
			var s = _box.style
			rememberPosition = {top: s.top, left: s.left, position: s.position}
			s.position = 'absolute'
			
			mouseOffset = offset
		}
			
		this.hide = function() {
			_box.style.display = 'none' 
		}
		
		this.show = function() {
			_box.style.display = '' 
		}
		
		this.onDragMove = function(x, y) {
			_box.style.top =  y - mouseOffset.y +'px'
			_box.style.left = x - mouseOffset.x +'px'
		}
		
		this.onDragSuccess = function(dropTarget) { }
		
		this.onDragFail = function() {
			/*
			var s = element.style
			s.top = rememberPosition.top
			s.left = rememberPosition.left
			s.position = rememberPosition.position
			*/
		}
		
		this.toString = function() {
			return _box.id
		}
	}
	
	function fixEvent(e){
	
		e = e || window.event
	
		if ( e.pageX == null && e.clientX != null ) {
			var html = document.documentElement
			var body = document.body
			e.pageX = e.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0)
			e.pageY = e.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0)
		}
	
		if (!e.which && e.button) {
			e.which = e.button & 1 ? 1 : ( e.button & 2 ? 3 : ( e.button & 4 ? 2 : 0 ) )
		}
	
		return e
	}
	
	function getOffset(elem){
		if (elem.getBoundingClientRect) {
			return getOffsetRect(elem)
		} else {
			return getOffsetSum(elem)
		}
	}
	
	function getOffsetRect(elem){
		
		var box = elem.getBoundingClientRect()
	 
		var body = document.body
		var docElem = document.documentElement
	 
		var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop
		var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft
		var clientTop = docElem.clientTop || body.clientTop || 0
		var clientLeft = docElem.clientLeft || body.clientLeft || 0
		var top  = box.top +  scrollTop - clientTop
		var left = box.left + scrollLeft - clientLeft
	 
		return { top: Math.round(top), left: Math.round(left) }
	}
	
	function getOffsetSum(elem){
		var top=0, left=0
		while(elem) {
			top = top + parseInt(elem.offsetTop)
			left = left + parseInt(elem.offsetLeft)
			elem = elem.offsetParent        
		}
	 
		return {top: top, left: left}
	}
	
	var dragMaster = (function() {
		
		var dragObject
		var mouseDownAt
	
		var currentDropTarget
		
		function mouseDown(e) {
			e = fixEvent(e)
			if (e.which!=1) return
	
			mouseDownAt = { x: e.pageX, y: e.pageY, element: this }
	
			addDocumentEventHandlers()
	
			return false
		}
	
		function mouseMove(e){
			e = fixEvent(e)
			if (mouseDownAt) {
				if (Math.abs(mouseDownAt.x-e.pageX)<5 && Math.abs(mouseDownAt.y-e.pageY)<5) {
					return false
				}
				var elem  = mouseDownAt.element
				dragObject = elem.dragObject
				var mouseOffset = getMouseOffset(elem, mouseDownAt.x, mouseDownAt.y)
				mouseDownAt = null
				
				dragObject.onDragStart(mouseOffset)
				
			}
			dragObject.onDragMove(e.pageX, e.pageY)
			var newTarget = getCurrentTarget(e)
			
			if (currentDropTarget != newTarget) {
				if (currentDropTarget) {
					currentDropTarget.onLeave()
				}
				if (newTarget) {
					newTarget.onEnter()
				}
				currentDropTarget = newTarget
	
			}
			return false
		}
			
		function mouseUp(){
			if (!dragObject) {
				mouseDownAt = null
			} else {
				if (currentDropTarget) {
					currentDropTarget.accept(dragObject)
					dragObject.onDragSuccess(currentDropTarget)
				} else {
					dragObject.onDragFail()
				}
	
				dragObject = null
			}
	
			removeDocumentEventHandlers()
		}
	
		function getMouseOffset(target, x, y) {
			var docPos	= getOffset(target)
			return {x:x - docPos.left, y:y - docPos.top}
		}
		
		function getCurrentTarget(e) {
			
			if (navigator.userAgent.match('MSIE') || navigator.userAgent.match('Gecko')) {
				var x=e.clientX, y=e.clientY
			} else {
				var x=e.pageX, y=e.pageY
			}
			dragObject.hide()
			var elem = document.elementFromPoint(x,y)
			dragObject.show()
	
			while (elem) {
				if (elem.dropTarget && elem.dropTarget.canAccept(dragObject)) {
					return elem.dropTarget
				}
				elem = elem.parentNode
			}
			return null
		}
	
		function addDocumentEventHandlers() {
			document.onmousemove = mouseMove
			document.onmouseup = mouseUp
			document.ondragstart = document.body.onselectstart = function() {return false}
		}
		function removeDocumentEventHandlers() {
			document.onmousemove = document.onmouseup = document.ondragstart = document.body.onselectstart = null
		}
	
		return {
	
			makeDraggable: function(element){
				element.onmousedown = mouseDown
			}
		}
	}())
	
	
	/**
	 * SDW main instance
	 */
	
	 
	return {
	
	// defaults
	"cfg": {
		"multiple": false,
		"fade": true,
		"curtains": true,
		"drag": true,
		"fadeColor": "#000",
		"instancePrefix": "sdw",
		"startZIndex": 5000,
		"opacity": 0.5,
		"speed": 10,
		"position": "fixed",
		"controls": true,
		// CSS3 shadow
		"shadow": "3px 3px 5px rgba(50, 50, 50, 0.21)",
		// box template
		"template": "<div style='width: 400px;position: relative;background: #F5F5F5;border: 1px solid #ccc;padding: 4px 0'><div style='background: #f5f5f5;text-align: right;padding-bottom: 3px;padding-right: 4px' sdw='dragbar'><table width='100%' margin='0' padding='0'><tr><td width='10%'></td><td width='70%' style='vertical-align: middle;text-align: center;color: #444'>Security alert</td><td width='20%' style='text-align: right'><div style='width: 50px;height: 25px;background: #555;color: #fff;cursor: pointer;top: 1px;right: 1px;display: inline-block' sdw='close'><div style='display: table-cell;vertical-align: middle;width: 50px;height: 25px;text-align: center'>x</div></div></td></tr></table></div><div style='font-size: 18px;color: #222;border: 1px solid #ccc;background: #fff;padding: 40px;margin: 0 4px' sdw='content'></div><div style='text-align: center;margin-top: 3px'><input type='button' style='display: inline-block;padding: 7px 30px;background: #fff;border: 1px solid #ccc' sdw='ok' value='Ok'><input type='button' style='display: inline-block;padding: 7px 30px;background: #fff;border: 1px solid #ccc;margin-left: 3px' sdw='cancel' value='Cancel'></div></div>"		
	},
	"id": function(a){
		if (a) return document.getElementById(a); else return false;
	},
	"fadeEffect": function(id, flag, target, callback, speed){
		var z = this;
		var _callback = callback;
				
		function tween(){
			if(z.alpha == z.target){
				clearInterval(z.elem.si);
				if (_callback) _callback();
			} else {
				var value = Math.round(z.alpha + ((z.target - z.alpha) * .05)) + (1 * z.flag);
				z.elem.style.zoom = '1';
				z.elem.style.opacity = value / 100;
				z.elem.style.filter = 'alpha(opacity=' + value + ')';
				z.alpha = value;
			}
		}
		
		z.elem = document.getElementById(id);
		clearInterval(z.elem.si);
		z.target = target ? target : flag ? 100 : 0;
		z.flag = flag || -1;
		z.alpha = z.elem.style.opacity ? parseFloat(z.elem.style.opacity) * 100 : 0;
		if (!this.cfg["speed"]) var _speed = 20; else var _speed = this.cfg["speed"];
		z.elem.si = setInterval(function(){tween()}, _speed);
	},
	"show": function(){
		var z = this;
		var id = this.createInstance();
		var content = this.renderHTML(id);
		var center = this.calculate(id);
		var el = this.id(id);
		
		if (this.cfg["curtains"]) this.makeCurtains();
		
		el.style.left = center["left"] + 'px';
		el.style.top = center["top"] + 'px';
		
		if (this.cfg["fade"]){
			el.style.opacity = 0;
			el.style.filter = 'alpha(opacity=0)';
		}
		el.style.visibility = 'visible';
		
		if (this.cfg["shadow"]) this.makeShadow(id);
		
		if (this.cfg["fade"]){
			this.fadeEffect(id, 1, '', function(){
				z.makeHideAction(id);
			})
		} else {
			z.makeHideAction(id);
		}
		
		if (this.cfg["drag"]) {
			this.makeDragBar(id);
		}
	},
	"makeHideAction": function(id){
		var _id = id;
		var z = this;
		
		var btn = this.searchNodeByClass(id, 'close');
		if (typeof btn == 'object'){
			btn.onclick = function(){
				z.hide(_id);
			}
		}
		
	},
	"searchNodeByClass": function(parentNodeId, attr){
		var _id = parentNodeId, z = this, match, _attr = attr;
		
		function allDescendants(node){
			for (var i = 0; i < node.childNodes.length; i++){
				var child = node.childNodes[i];
				allDescendants(child);
				if (child.getAttribute){
					if (child.getAttribute("sdw") == _attr){
						match = child;
						break;
					}
				}
			}
			
			if (!match) return false; else return match;
		}
		allDescendants(this.id(parentNodeId));
		return match ? match : false;
	},
	"hide": function(id){
		var a = window._sdw;
		var b = a.indexOf(id);
		var el = this.id(id);
		var hide = this.searchNodeByClass(id, 'close');
		
		if (b != -1){
			a.splice(b);
			var c = this.cfg["instancePrefix"] + "_" + "curtains";
			var z = this;
			el.style.opacity = 0;
			el.style.filter = 'alpha(opacity=0)';
			if (hide){
				hide.onclick = function() {}
			}
			// remove curtains and box
			if (z.id(c)){
				//z.id(c).style.visibility = "hidden";
				z.id(c).parentNode.removeChild(z.id(c))
				el.parentNode.removeChild(el)
			}
		}
	},
	"calculate": function(id){
		var viewport = this.fullViewport();

		// element coords
		var el = this.id(id);
		var leftOffset = (viewport["w"] / 2) - (el.offsetWidth / 2);
		
		// top scroll
		var scroll = this.getPageScroll();
		var clientOffset = this.clientViewport()
		
		var topOffset  = ((clientOffset["h"] / 2) - (el.offsetHeight / 2)) + scroll["top"];
		return {"left": leftOffset, "top": topOffset}
	},
	"clientViewport": function(){
	
		 var viewportwidth;
		 var viewportheight;

		 if (typeof window.innerWidth != 'undefined')
		 {
			  viewportwidth = window.innerWidth,
			  viewportheight = window.innerHeight
		 } else if (typeof document.documentElement != 'undefined'
			 && typeof document.documentElement.clientWidth !=
			 'undefined' && document.documentElement.clientWidth != 0){
			   viewportwidth = document.documentElement.clientWidth,
			   viewportheight = document.documentElement.clientHeight
		 } else {
			   viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
			   viewportheight = document.getElementsByTagName('body')[0].clientHeight
		 }
		 
		 return {"h": viewportheight, "w": viewportwidth}
	},
	"getPageScroll": function(){
		var getPageScroll = (window.pageXOffset != undefined) ?
		  function() {
			return {
			  left: pageXOffset,
			  top: pageYOffset
			};
		  } :
		  function() {
			var html = document.documentElement;
			var body = document.body;
		
			var top = html.scrollTop || body && body.scrollTop || 0;
			top -= html.clientTop;
		
			var left = html.scrollLeft || body && body.scrollLeft || 0;
			left -= html.clientLeft;
		
			return { top: top, left: left };
		  }
		  
		  return getPageScroll()
  	},
	"makeCurtains": function(){
		var viewport = this.fullViewport();
		var h = viewport["h"];
		var w = viewport["w"];
		
		var el = document.createElement("div");
		el.id = this.cfg["instancePrefix"] + "_" + "curtains";
		el.style.position = "absolute";
		el.style.background = this.cfg["fadeColor"];

		el.style.top = 0;
		el.style.left = 0;
		el.style.padding = 0;
		el.style.margin = 0;
		el.style.width = w + "px";
		el.style.height = h + "px";
		
		el.style.opacity = this.cfg.opacity / 10;
		el.style.filter = "alpha(opacity=" + this.cfg["opacity"] * 10 + ")";
		
		document.body.insertBefore(el, document.body.childNodes[0]);
	},
	"fullViewport": function(){
		
		var D = document;
		
		function getDocHeight(){
			
			var D = document;

				return Math.max(
					D.body.scrollHeight, D.documentElement.scrollHeight,
					D.body.offsetHeight, D.documentElement.offsetHeight,
					D.body.clientHeight, D.documentElement.clientHeight
				);
		}
		
		function getDocWidth(){
			return Math.max(
				D.body.clientWidth, D.documentElement.clientWidth
			);
		}
		
		var viewportW = getDocWidth();
		var viewportH = getDocHeight();
		
		return {"w": viewportW, "h": viewportH}
	},
	"renderHTML": function(id){
		var box, pos;
		if (!this.id(id)){
			if (typeof document.body != 'object') throw "No <body> tag found";
			box = document.createElement('div');
			box.id = id;
			box.style.position = 'absolute';
			box.style.zIndex = this.cfg["startZIndex"] + 1000;
			box.innerHTML = this.cfg["template"];
			box.style.visibility = 'hidden';
			document.body.insertBefore(box, document.body.childNodes[0]);
			this.makeContent(id);
		}
	},
	"makeContent": function(id){
		
		var el;
		
		if (this.cfg["template"]){
			el = this.searchNodeByClass(id, 'content');
			if (el){
				el.innerHTML = this.cfg["message"];
			}
		} else {
			alert("sdw: no content");
		}
	},
	"createInstance": function(){
		
		var default_name = this.cfg.instancePrefix;
		
		if (typeof window._sdw != 'array') window._sdw = [];

		if (window._sdw.length > 0){
			if (this.cfg['multiple']){
				// avoid instance name collision				
				function checkCollision(name){
					for (var a in window._sdw){
						if (window._sdw[a] == name) var match = true;
					}
					return match ? true : false;
				}
				
				if (checkCollision(default_name)){
					for (var b = 0; b < 200; b++){
						var c = default_name + b.toString();
						
						if (!checkCollision(c)){
							default_name = c;
							var flag = true;
						}
						if (flag) break;
					}
				}
			}
		} else {
			window._sdw.push(default_name);
		}
		
		return default_name
	},
	"set": function(config){
		if (typeof config == 'object'){
			for (var a in config){
				this.cfg[a] = config[a];
			}
		}
	},
	"makeDragBar": function(id){
		
		var el = this.searchNodeByClass(id, 'dragbar');
		
		if (el){
			new DragObject(el, this.id(id));
		}
	},
	"makeShadow": function(id)
	{
		function getsupportedprop(proparray)
		{
			var root = document.documentElement;
			for (var i=0; i < proparray.length; i++){
				if (proparray[i] in root.style){
					return proparray[i]
				}
			}
		}
		
		var boxshadowprop = getsupportedprop(['boxShadow', 'MozBoxShadow', 'WebkitBoxShadow']);
		this.id(id).style[boxshadowprop]=this.cfg["shadow"];
	}
	
	
	}
}

